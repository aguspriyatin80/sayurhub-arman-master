const { Product } = require('../models/product');
const { Cart } = require('../models/cart');
const { User } = require('../models/user');

exports.addToCart = async(req, res, next) => {
    try {
        var productId = req.params.product_id;
        var userId = req.userData._id;
        console.log(userId);
        Cart.findOne({ user: userId }, (err, doc) => {
            if (err) res.send(err);
            if (doc == null) {
                var cart = new Cart;
                Product.findById(productId, (err, product) => {
                    if (err) console.error(err);
                    let item = {
                        id: req.params.product_id,
                        name: product.product_name,
                        image: product.product_image,
                        price: product.actualPrice,
                        weight: product.weight,
                        stock: product.stock,
                        subtotal: product.actualPrice * 1,
                        quantity: 1
                    }
                    cart.items.push(item);
                    cart.user = userId; // set cart user
                    cart.totalWeight = item.weight * item.quantity;
                    cart.totalPrice = item.price * item.quantity;
                    cart.totalQty = item.quantity;
                    cart.save((err, data) => {
                        if (err) res.send(err);
                        res.json(cart);
                    });
                });
            } else {
                Product.findById(productId, (err, product) => {
                    let item = {
                        id: req.params.product_id,
                        name: product.product_name,
                        image: product.product_image,
                        price: product.actualPrice,
                        weight: product.weight,
                        stock: product.stock,
                        subtotal: product.actualPrice * 1,
                        quantity: 1
                    }
                    doc.items.push(item);
                    doc.save(err, data => {
                        res.json(doc);
                    })
                    doc.totalWeight += item.weight * item.quantity;
                    doc.totalQty += item.quantity;
                    doc.totalPrice += item.price * item.quantity;
                })
            }
        })
    } catch (error) {
        next(error)
    }
}

exports.updateCart = async(req, res, next) => {
    try {
        const userId = req.userData._id
        const quantity = req.body.quantity;
        const query = { _id: req.params.id, "items.id": req.params.product_id }
        const updateDocument = {
            $set: {
                "items.$.quantity": parseInt(quantity)
            },
            $set: { totalQty: totalQty + items.$.quantity }
        };
        let update = await Cart.updateOne(query, updateDocument);

        // let find = await Cart.findOne ({user:userId})
        // console.log(find);
        const cart = await Cart.findOne({ user: userId })
        console.log(cart);


        // cart.map((X)=>{
        //     console.log(X.items);
        // (X.items).forEach((item)=>{
        //     if (item.product===productId){
        //         item.quantity += 1;
        //     }
        // })
        // })
        res.status(200).json({
            success: true,
            msg: "Cart updated!",
            update
        });
    } catch (error) {
        next(error)
    }
}


exports.getCart = async(req, res) => {
    try {
        let cart = await Cart.find()
        if (!cart) {
            return res.status(400).json({
                type: "Invalid",
                msg: "Cart not found",
            })
        }
        res.status(200).json({
            status: true,
            cart
        })
    } catch (err) {
        console.log(err)
        res.status(400).json({
            type: "Invalid",
            msg: "Something went wrong",
            err: err
        })
    }
}
exports.getCartUser = async(req, res) => {
    try {
        var userId = req.userData._id;
        // console.log(userId);
        let cart = await Cart.findOne({ user: userId })
        if (!cart) {
            return res.status(400).json({
                type: "Invalid",
                msg: "Cart not found",
            })
        }
        res.status(200).json({
            status: true,
            cart
        })
    } catch (err) {
        console.log(err)
        res.status(400).json({
            type: "Invalid",
            msg: "Something went wrong",
            err: err
        })
    }
}

exports.emptyCart = async(req, res) => {
    try {
        const { id } = req.params;


        await Cart.findByIdAndRemove(id, (err, doc, result) => {
            if (err) {
                throw "Failed to delete product"
            }
            if (!doc) {
                res.status(404).json({
                    success: false,
                    msg: "Product not found"
                })
            }
            res.status(200).json({
                success: true,
                msg: "Product deleted!",
                doc
            });
        });
    } catch (err) {
        console.log(err)
        res.status(400).json({
            type: "Invalid",
            msg: "Something went wrong",
            err: err
        })
    }
}

exports.deleteProductCart = async(req, res) => {
    try {
        // let cart_id = req.params.id
        // let id = req.params.product_id;
        //     var userId = req.userData;

        //  let cart = await Cart.findOne( { _id: cart_id })

        //     for(let i = 0; i < cart.items.length; i++) {
        //         let item = cart.items[i];
        //         if(item.id === id) {
        //             console.log([item.id]);
        //            cart.items.splice(i, 1);
        //         }
        //     }
        var productId = req.params.product_id;

        let product = await Cart.findOneAndUpdate({ _id: req.params.id }, { $pull: { 'items': { 'id': productId } } }, { new: true });



        res.status(200).json({

            success: true,
            msg: "Successfully retrieve product data",
            product

        })
    } catch (err) {
        console.log(err)
        res.status(400).json({
            type: "Invalid",
            msg: "Something went wrong",
            err: err
        })
    }
};