const mongoose = require('mongoose');
const { Schema } = mongoose;

const discussionSchema = new Schema(
    {
        write: {
            type: String,
            require: true
        },
        product: { type: Schema.Types.ObjectId, ref: "Product", default: null },
        user: { type: Schema.Types.ObjectId, ref: "User", default: null },
        // admin: { type: Schema.Types.ObjectId, ref: "Admin", default: null}

    },
    {
        timestamps: true
    }
)

const discussion = mongoose.model("Discussion", discussionSchema);

exports.Discussion = discussion;
