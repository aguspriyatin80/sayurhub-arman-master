const express = require("express");
const router = express.Router();
const { Authentication } = require('../middlewares/auth');

const cartControllers = require("../controllers/cart");

router.post("/add/:product_id", Authentication, cartControllers.addToCart);
router.put("/update/:id/:product_id", Authentication, cartControllers.updateCart);
router.get("/list",cartControllers.getCart)
router.get("/view", Authentication,cartControllers.getCartUser)
router.delete("/empty/:id", Authentication, cartControllers.emptyCart);
router.delete("/delete/:id/:product_id", Authentication,cartControllers.deleteProductCart)

module.exports = router;
