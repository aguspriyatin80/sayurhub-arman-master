const express = require("express");
const router = express.Router();

const discussionController = require("../controllers/discussion");
const replyController = require("../controllers/reply");
const { Authentication, IsAdmin } = require("../middlewares/auth");
// const {  } = require("../middlewares/auth")


router.get('/:product_id', discussionController.GetDiscussionByProduct);
router.get('/reply/:discussion_id', replyController.GetReply);
router.post('/create/:product_id', Authentication, discussionController.CreateUser);
router.post('/reply/:discussion_id', IsAdmin, replyController.Reply);
router.put('/edit/:id', Authentication, discussionController.EditDiscussion);
router.put('/reply/edit/:id', IsAdmin, replyController.EditReply);
router.delete('/delete/:id', Authentication, discussionController.DeleteDiscussion);
router.delete('/reply/delete/:id', IsAdmin, replyController.DeleteReply);

module.exports = router;
