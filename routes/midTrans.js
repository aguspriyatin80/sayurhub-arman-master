const express = require("express");
const router = express.Router();

const midtransController = require('../controllers/midTrans')

router.post('/payment',midtransController.getToken)

module.exports = router
