const express = require("express");
const router = express.Router();

const transactionController = require("../controllers/transaction");
const { Authentication, IsAdmin } = require("../middlewares/auth");

router.post("/checkout/:cart_id",Authentication, transactionController.Create);

router.get("/find",Authentication, transactionController.TransactionById);
//router.post("/charges",Authentication, transactionController.stripeCharge);
// router.put("/update/:cart_id",Authentication, transactionController.Edit);
//router.delete("/delete/:cart_id",Authentication, transactionController.Delete);

module.exports = router;
